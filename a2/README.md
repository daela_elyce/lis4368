> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web App Development

## Daela Lyewsang-Holness

### Assignment 2 Requirements:

*Three Parts:*

1. Write Database Servlet
2. Install MySQL JDBC Driver
3. Deploying Servlet using @WebServlet (using tomcat 9)


#### Assignment Screenshots:
[http://localhost:9999/hello/index.html]

![Screenshot of hello index](img/thishomescreen.PNG)

[http://localhost:9999/hello/sayhello]

![Hello World with random number ](img/randomnumpic.PNG)

[http://localhost:9999/hello/querybook.html]

![Query Form](img/yetanother.PNG)

[http://localhost:9999/hello/sayhi]
![Deploying Servlet using @WebServlet](img/hiagain.PNG)

![Boxes Shown are selected](img/boxselect.PNG)

![Query Results](img/thankyouq.PNG)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/daelaelyce/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
