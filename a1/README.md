> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advance Mobile App Development

## Daela Lyewsang-Holness

### Assignment 1 Requirements:

*Three Parts:*

1. Distrbuted Version Control with Git and Bitbucket
2. Java/SP/Servlet Development Instalation 
3. Chapter Questions(Chs1-4)

#### README.md file should include the following items:

* Screenshot of runing java Hello(#1 above)
* Screenshot of running http//:localhost9999
* git commands with short descriptions
* Bitbucket repo links

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init-initialize and empty git repo
2. git status- displays state of working directory
3. git add- adds files
4. git commit- commits files to local repo
5. git push- uploads local content to remote repo
6. git pull- fetches and downloads files from remote repo to local repo
7. git remote- *synchs changes*

#### Assignment Screenshots:

![Screenshot of  http://localhost](img/tomscreeenshot.jpg)

![AMPPS Installation Screenshot](img/ammpsinstall.jpg)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/daelaelyce/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
