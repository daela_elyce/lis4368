-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema dl18cd
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `dl18cd` ;

-- -----------------------------------------------------
-- Schema dl18cd
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `dl18cd` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `dl18cd` ;

-- -----------------------------------------------------
-- Table `dl18cd`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dl18cd`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `dl18cd`.`customer` (
  `cust_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cus_frame` VARCHAR(15) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` INT NOT NULL,
  `cus_phone` BIGINT NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(6,2) NOT NULL,
  `cus_total_sales` DECIMAL(6,2) NOT NULL,
  `cus_roles` VARCHAR(225) NOT NULL,
  PRIMARY KEY (`cust_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `dl18cd`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dl18cd`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `dl18cd`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `cus_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM("m", "f") NOT NULL,
  `pet_cost` DECIMAL(6,2) NOT NULL,
  `pet_price` DECIMAL(6,2) NOT NULL,
  `pet_age` TINYINT NOT NULL,
  `pet_color` VARCHAR(45) NOT NULL,
  `pet_sale_date` DATE NOT NULL,
  `pet_vaccine` ENUM("y", "n") NOT NULL,
  `pet_neuter` ENUM("y", "n") NOT NULL,
  `pet_notes` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`pet_id`),
  CONSTRAINT `fk_pet_customer1`
    FOREIGN KEY (`cus_id`)
    REFERENCES `dl18cd`.`customer` (`cust_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `dl18cd`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dl18cd`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `dl18cd`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` INT(9) NOT NULL,
  `pst_phone` BIGINT NOT NULL,
  `pet_email` VARCHAR(100) NOT NULL,
  `pet_url` VARCHAR(100) NOT NULL,
  `pet_vtd_sales` DECIMAL(10,2) NOT NULL,
  `pet_notes` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `dl18cd`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `dl18cd`;
INSERT INTO `dl18cd`.`customer` (`cust_id`, `cus_frame`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_roles`) VALUES (DEFAULT, 'bob', 'dylan', '555', 'london', 'Alabama', 33313, 555555555, 'doggie@getapet.com', 12, DEFAULT, DEFAULT);
INSERT INTO `dl18cd`.`customer` (`cust_id`, `cus_frame`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_roles`) VALUES (DEFAULT, 'janis', 'joplin', '2222', 'milan', 'Arizona', 33312, 888888888, 'doggie@getapet.com', 15, 205, 'grest');
INSERT INTO `dl18cd`.`customer` (`cust_id`, `cus_frame`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_roles`) VALUES (DEFAULT, 'bob', 'marley', '33333', 'paris', 'Alaska', 33314, 666666666, 'doggie@getapet.com', 84, 814, 'bad');
INSERT INTO `dl18cd`.`customer` (`cust_id`, `cus_frame`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_roles`) VALUES (DEFAULT, 'kendric', 'lamar', '6666', 'brussels', 'Colorado', 3316, 999999999, 'doggie@getapet.com', 99, 754, 'good');
INSERT INTO `dl18cd`.`customer` (`cust_id`, `cus_frame`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_roles`) VALUES (DEFAULT, 'hendrix', 'uncle', '7777', 'Lagos', 'West Virgina', 33315, 777777777, 'doggie@getapet.com', 55, 5545, 'hmmm');
INSERT INTO `dl18cd`.`customer` (`cust_id`, `cus_frame`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_roles`) VALUES (DEFAULT, 'paul', 'mc', '999', 'Capetown', 'Connecticut', 33319, 444444444, 'doggie@getapet.com', 44, 5545, 'son');
INSERT INTO `dl18cd`.`customer` (`cust_id`, `cus_frame`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_roles`) VALUES (DEFAULT, 'ringo', 'star', '8888', 'Miami', 'Delware', 33316, 222222222, 'doggie@getapet.com', 2, 79796, 'daugter');
INSERT INTO `dl18cd`.`customer` (`cust_id`, `cus_frame`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_roles`) VALUES (DEFAULT, 'john', 'lennon', '33333', 'Ontario', 'Georgia', 8654, 888888888, 'doggie@getapet.com', 3, 2426, 'kids');
INSERT INTO `dl18cd`.`customer` (`cust_id`, `cus_frame`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_roles`) VALUES (DEFAULT, 'sly', 'robbie', '4444', 'Kingston', 'Montana', 84512, 555555555, 'doggie@getapet.com', 2, 2545, 'husband');
INSERT INTO `dl18cd`.`customer` (`cust_id`, `cus_frame`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_roles`) VALUES (DEFAULT, 'Ella', 'Fitzgerald', '5555', 'Memphis', 'California ', 87652, 111111111, 'doggie@getapet.com', 5, 6466, 'related');

COMMIT;


-- -----------------------------------------------------
-- Data for table `dl18cd`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `dl18cd`;
INSERT INTO `dl18cd`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, DEFAULT, NULL, 'dogv', 'female', 2000, 23 , 5, 'blue', '2/25/19', 'y', 'n', DEFAULT);
INSERT INTO `dl18cd`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, DEFAULT, NULL, 'cat', 'female', 15, 30, 1, 'pink', '2/30/19', 'y', 'n', DEFAULT);
INSERT INTO `dl18cd`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, DEFAULT, NULL, 'bird', 'female', 200, 250, 2, 'purple', '2/25/19', 'y', 'n', DEFAULT);
INSERT INTO `dl18cd`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, DEFAULT, NULL, 'walrus', 'female', 8000, 81000, 4, 'red', '2/25/19', 'y', 'n', DEFAULT);
INSERT INTO `dl18cd`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, DEFAULT, NULL, 'boa', 'male', 5000, 5000, 5, 'blue', '2/25/19', 'y', 'n', DEFAULT);
INSERT INTO `dl18cd`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, DEFAULT, NULL, 'penguin', 'male', 6000, 6002, 10, 'black', '2/25/19', 'y', 'n', DEFAULT);
INSERT INTO `dl18cd`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, DEFAULT, NULL, 'elephant', 'male', 10000, 100001, 20, 'white', '2/25/19', 'y', 'n', DEFAULT);
INSERT INTO `dl18cd`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, DEFAULT, NULL, 'chicken', 'male', 10, 5, 1, 'red', '2/25/19', 'y', 'n', DEFAULT);
INSERT INTO `dl18cd`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, DEFAULT, NULL, 'duck', 'male', 10, 5, 1, 'yellow', '2/25/19', 'y', 'n', DEFAULT);
INSERT INTO `dl18cd`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, DEFAULT, NULL, 'money', 'male', 2500, 3000, 2, 'red', '2/25/19', 'y', 'n', DEFAULT);


START TRANSACTION;
USE `dl18cd`;
INSERT INTO `dl18cd`.`petstore` ('pst_id','pst_name','pst_street','pst_city','pst_state','pst_zip','pst_phone','pst_email','pst_url','pst_ytd_sales','pst_notes') VALUES (1,"Pets R Us","123 South Street",Tallahassee,fl,32304,8501234567,pets@mail.com,petsrus.com,34000.00,NULL);
INSERT INTO `dl18cd`.`petstore` ('pst_id','pst_name','pst_street','pst_city','pst_state','pst_zip','pst_phone','pst_email','pst_url','pst_ytd_sales','pst_notes') VALUES (3,"We Have Pets","213 That Way",Atlanta,ga,30303,4049834265,wehave@mail.com,wehavepets.com,23000.00,NULL);
INSERT INTO `dl18cd`.`petstore` ('pst_id','pst_name','pst_street','pst_city','pst_state','pst_zip','pst_phone','pst_email','pst_url','pst_ytd_sales','pst_notes') VALUES (4,"Take R Critters","954 Yello Brick Rd","New York",ny,10001,7188351232,taker@mail.com,takercritters.com,102000.00,NULL);
INSERT INTO `dl18cd`.`petstore` ('pst_id','pst_name','pst_street','pst_city','pst_state','pst_zip','pst_phone','pst_email','pst_url','pst_ytd_sales','pst_notes') VALUES (5,"Dogz N Catz","932 North Way","Saint Petersburg",fl,33713,7272123418,dogzncatz@mail.com,dogzncatz.com,36000.00,NULL);
INSERT INTO `dl18cd`.`petstore` ('pst_id','pst_name','pst_street','pst_city','pst_state','pst_zip','pst_phone','pst_email','pst_url','pst_ytd_sales','pst_notes') VALUES (6,"The Wet Dog","2011 Johnson St",Jacksonville,fl,32034,9046659982,wetdog@mail.com,thewetdog.com,34500.00,NULL);
INSERT INTO `dl18cd`.`petstore` ('pst_id','pst_name','pst_street','pst_city','pst_state','pst_zip','pst_phone','pst_email','pst_url','pst_ytd_sales','pst_notes') VALUES (7,"Wag A Tail","351 South Central",Tallahassee,fl,32304,8505565554,wag@mail.com,wagatail.com,26500.00,NULL);
INSERT INTO `dl18cd`.`petstore` ('pst_id','pst_name','pst_street','pst_city','pst_state','pst_zip','pst_phone','pst_email','pst_url','pst_ytd_sales','pst_notes') VALUES (8,"Pet Shoppe","512 Main Street",Miami,fl,33101,3054434487,theshoppe@mail.com,petshoppe.com,49500.00,NULL);
INSERT INTO `dl18cd`.`petstore` ('pst_id','pst_name','pst_street','pst_city','pst_state','pst_zip','pst_phone','pst_email','pst_url','pst_ytd_sales','pst_notes') VALUES (9,"Bark Fest","8443 Conn Rd",Atlanta,ga,30303,4049982231,bark@mail.com,barkfest.com,55500.00,NULL);
INSERT INTO `dl18cd`.`petstore` ('pst_id','pst_name','pst_street','pst_city','pst_state','pst_zip','pst_phone','pst_email','pst_url','pst_ytd_sales','pst_notes') VALUES (10,"Ankle Biters","1212 Flower St",Jacksonville,fl,32034,9045459133,ankelbit@mail.com,anklebitters.com,100000.00,NULL);



COMMIT;

