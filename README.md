> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 4368 Advance Web Applications Development

## Daela Lyewsang

### Class Number Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPS
    - Install  Tomcat
    - Install JDK
    - Provide screnshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Write a Database Servlet
    - Setup a Database on MySQ
    - Install MySQL JDBC Driver
    - Write a Client-side HTML Form
    - Write the Server-side Database Query Servlet
    - Configure the Request URL for the Servlet
    - Deploying Servlet using @WebServlet

3. [A3 README.md](a3/README.md "My A3 README.md file")
    -Create and ERD
    -

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Server side validation
    - Displays Entered data
    - Validates for correct data


5. [P1 README.md](p1/README.md "My P1 README.md file")
    - Client Side Validations
    - Includes Number, Case Commas, hyphens, decimals

6. [P2 README.md](p2/README.md "My P1 README.md file")
    - Client Side Validation
    - Includes Number, Case Commas, hyphens, decimals
    - Connects to database
    - allows delete, add, and modify
