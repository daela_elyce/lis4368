> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web App Development

## Daela Lyewsang-Holness

### Project Requirements:
- Server Side Side Validation



#### Assignment Screenshot and Links:
|*Failed server side Validation*:  |  *Passed server Side Validation*: |
|-----------------------------:|:----------------------------------|
|![P1 Carousel](img/failed.PNG "Failed server side validaton") | ![P1 Failed](img/passed.PNG "Passed Server Side Validation") |







#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/daelaelyce/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
