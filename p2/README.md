> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web App Development

## Daela Lyewsang-Holness

### Project Requirements:
* Screenshot of Valid User Form Entry
* Screenshot of passed validation
* Screenshot of Display data
* Screenshot of Modify form
* Screenshot of Modified data 
* Screenshot of Delete Warning
* Screenshot of Associated Database Changes  




#### Assignment Screenshot and Links:
| *Screenshot of Valid User Form Entry*: |  *Screenshot of passed validation* |
|-----------------------------:|:----------------------------------|
| ![P1 Carousel](img/1.png "Failed server side validaton") | ![P1 Failed](img/2.png "Passed Server Side Validation") |

| *Screenshot of Display data* |  *Screenshot of Modify form* |
|-----------------------------:|:----------------------------------|
| ![P1 Carousel](img/3.png "Failed server side validaton") | ![P1 Failed](img/4.png "Passed Server Side Validation") |

| *creenshot of Modified data* |  *Screenshot of Delete Warning* |
|-----------------------------:|:----------------------------------|
| ![P1 Carousel](img/5.png "Failed server side validaton") | ![P1 Failed](img/6.png "Passed Server Side Validation") |

*Screenshot of Associated Database Changes*
![P1 Carousel](img/7.png "Failed server side validaton")





